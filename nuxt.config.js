export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    ssr: false,
    head: {
        title: "Admin Markaz",
        script: [],
        htmlAttrs: {
            lang: "en"
        },
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            },
            { hid: "description", name: "description", content: "" },
            { name: "format-detection", content: "telephone=no" }
        ],
        link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
        script: [
            { src: "https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js" }
        ],
        script: [
            {
                src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
                type: "text/javascript"
            }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        "@/globalCss/app.min.css",
        "@/globalCss/bootstrap.min.css",
        "@/globalCss/material-icons.css",
        "@/globalCss/styles.css",
        "@/globalCss/style.css",
        "@/globalCss/icon.scss"
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        "~plugins/vuetify",
        "@/plugins/vue-ripple",

        "@/plugins/axios",
        { src: "~/plugins/tinymce", ssr: false },
        { src: "@/plugins/vue-apexChart", ssr: false },
        { src: "@/plugins/vue2Pdf.js", mode: "client" },
        "@/plugins/vue-validate.js",
        "@/plugins/vueExcel.js"
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: ["@nuxtjs/fontawesome", "@nuxtjs/vuetify", "nuxt-pdf"],
    pdf: {
        // Change the format of the pdfs.
        format: "A4", // This is optional
        printBackground: true // Include background in pdf.
    },
    fontawesome: {
        component: "fa",
        icons: {
            solid: true,
            brands: true
        }
    },
    axios: {
        // baseURL: "http://e842a3dd150b.ngrok.io/api/",
        baseURL: "https://media.huquqiyportal.uz/api/"
        // baseURL: "http://ass.tujjor.org/api/",
        // baseURL: 'http://media.huquqiyportal.uz/api',
        // baseURL: "http://localhost:3301/api"
    },
    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/bootstrap
        "nuxt-material-design-icons",
        "@nuxtjs/axios",
        "@nuxtjs/auth-next",
        "@nuxt/http",
        "vue-toastification/nuxt",
        "nuxt-socket-io",
        [
            "dropzone-nuxt",
            {
                /* module options */
            }
        ]
    ],
    io: {
        // module options
        sockets: [
            {
                name: "admin",
                url: "https://media.huquqiyportal.uz"
            }
        ]
    },
    toast: {
        timeout: 3000,
        closeOnClick: false
    },

    router: {
        middleware: ["auth"]
    },
    auth: {
        redirect: {
            login: "/auth/login",
            logout: "/auth/login",
            callback: "/auth/login",
            home: "/"
        },
        strategies: {
            local: {
                token: {
                    property: "token"
                },
                user: {
                    property: "data"
                },
                endpoints: {
                    login: { url: "/user/login", method: "post" },
                    user: { url: "/user/me", method: "get" }
                }
            }
        }
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
};
