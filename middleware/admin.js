export default function(context) {
    let user = this.$auth.user.userType;

    if (user != "admin") {
        return context.redirect("/");
    }
}
