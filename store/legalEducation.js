import { getField, updateField } from "vuex-map-fields";

export const state = () => ({
  firstPage: {
    phones: 'wewed',
    email: '',
    site: '',
    type: '',
    mainImage: ''
  },
  secondPage: {
    title: {
      uz: '',
      ru: '',
      cy: ''
    },
    about: {
      uz: '',
      ru: '',
      cy: ''
    }
  },
  thirdPage: {
    address: {
      uz: '',
      ru: '',
      cy: ''
    },
    additional: {
      uz: '',
      ru: '',
      cy: ''
    }
  },
  fourthPage: {
    aboutStudyPlace: {
      uz: '',
      ru: '',
      cy: ''
    },
    socialLinks: {
      facebook: '',
      telegram: '',
      instagram: '',
      twitter: ''
    }
  },
  fivethPage: {
    images: [],
    video: null
  },
  sixthPage: {
    logo: '',
    director: {
      uz: '',
      ru: '',
      cy: ''
    },
    conviences: [
      {
        title: {
          uz: '',
          ru: '',
          cy: ''
        },
        description: {
          uz: '',
          ru: '',
          cy: ''
        }
      }
    ],
    leaders: '',
    teachers: '',
    workers: '',
    otherWorkers: '',
    students: '',
    map: ''
  }
})

// Mutations
export const mutations = {
  updateField,
  setPage (state, data) {
    if (data[1] == 'firstPage') {
      return (state.firstPage = data[0])
    }
    if (data[1] == 'secondPage') {
      return (state.secondPage = data[0])
    }
    if (data[1] == 'thirdPage') {
      return (state.thirdPage = data[0])
    }
    if (data[1] == 'fourthPage') {
      return (state.fourthPage = data[0])
    }
    if (data[1] == 'fivethPage') {
      return (state.fivethPage = data[0])
    }
    if (data[1] == 'sixthPage') {
      return (state.sixthPage = data[0])
    }
  },

}

//Action
export const actions = {
  async getAllData () {
    const response = await this.$axios.$get('/technical-school/all')
    console.log(response)
  },

  async sendAllData ({ state }) {
    const data = {
      ...state.firstPage,
      ...state.secondPage,
      ...state.thirdPage,
      ...state.fourthPage,
      ...state.firstPage,
      ...state.sixthPage
    }
   
    const response = await this.$axios.$post('/technical-school/add', data)
   if(response.success){
    this.$router.push('/legaleducation')
  }
},
}

//Getters
export const getters = {
  getField
}
